//
// Created by timur on 23.10.2019.
//

#ifndef WEBLAB_GENERATOR_H
#define WEBLAB_GENERATOR_H

#include <vector>
#include <utility>

#define Coeffs std::vector<unsigned int>
#define Coeff unsigned int
#define Gene unsigned int

class Genes : public std::vector<unsigned int>{
public:
    double fitness;
    unsigned int diff;
    bool isBest = false;
};

#define Population std::vector<Genes>
#define Parents std::pair<Genes, Genes>

class Generator {
public:
    Generator(const Coeffs &coeffs, unsigned int targetValue);

    Genes getGenes();
private:
    Coeffs _coeffs;
    unsigned int _targetValue;
    Population _currentPopulation;
    unsigned int _populationSize;

    void showPopulation();
    void randomisePopulation(unsigned int populationSize);
    Population getNewPopulation();
    void updateUtility(Population &population);
    Parents getParents(const Population &population);
    Genes getChild(const Parents &parents);
    unsigned int getDiff(const Genes &genes);
    void mutation(Genes &genes);
    Population selection(Population &population1, Population &population2);
};


#endif //WEBLAB_GENERATOR_H
