//
// Created by timur on 23.10.2019.
//

#include "Generator.h"
#include <iostream>
#include <random>

Generator::Generator(const Coeffs &coeffs, unsigned int targetValue) {
    _coeffs = coeffs;
    _targetValue = targetValue;
    _populationSize = coeffs.size();
}

Genes Generator::getGenes() {
    std::cout << "Genetic algorithm\n";
    std::cout << "-----------------\n";
    std::cout << "Task:\n";
    for (size_t i = 0; i < _coeffs.size()-1; ++i){
        std::cout << "X" << i+1 << " * " << _coeffs[i] << " + ";
    }
    std::cout << "X" << _coeffs.size() << " * " << *(_coeffs.end()-1) << " = " << _targetValue << "\n\n";
    randomisePopulation(_coeffs.size());
    showPopulation();
    Genes bestGenes;
    bool isFound = false;
    while (!isFound){
        _currentPopulation = getNewPopulation();
        for (Genes &genes : _currentPopulation){
            if (genes.isBest){
                isFound = true;
                bestGenes = genes;
            }
        }
        if (!isFound){
            showPopulation();
        }
    }
    std::cout << "The optimum genetic code is - ";
    for (Gene gene : bestGenes){
        std::cout << gene << " ";
    }
    std::cout << "\n";
    for (size_t i = 0; i < _coeffs.size()-1; ++i){
        std::cout << "(" << bestGenes[i] << " * " << _coeffs[i] << ")" << " + ";
    }
    std::cout <<  "(" << *(bestGenes.end()-1) << " * " << *(_coeffs.end()-1) << ")" << " = " << _targetValue << "\n\n";
    return bestGenes;
}

void Generator::randomisePopulation(unsigned int populationSize) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> distTarget(0,_targetValue);
    _currentPopulation.resize(_populationSize);
    for (Coeffs &coeffs : _currentPopulation){
        coeffs.resize(_coeffs.size());
        for (Coeff &coeff : coeffs){
            coeff = distTarget(rng);
        }
    }
    updateUtility(_currentPopulation);
}

void Generator::showPopulation() {
    unsigned int i = 0;
    std::cout << "\nCurrent population:\n";
    for (Genes &genes : _currentPopulation){
        std::cout.width(2);
        std::cout << i+1 << ") ";
        for (Gene &gene : genes){
            std::cout.width(2);
            std::cout << gene << " ";
        }
        ++i;
        std::cout.width(20);
        std::cout << "diff = ";
        std::cout.width(3);
        std::cout << genes.diff;
        std::cout.width(20);
        std::cout.precision(2);
        std::cout << "fitness = " << genes.fitness << "%";
        std::cout << "\n";
    }
}

Population Generator::getNewPopulation() {
    Population population;

    std::cout << "\nCrossover + mutation:\n";
    Population childPopulation;
    for (size_t i = 0; i < _populationSize; i++){
        Parents parents = getParents(_currentPopulation);
        std::cout << i+1 << ") ";
        for (const Gene &gene : parents.first){
            std::cout.width(2);
            std::cout << gene << " ";
        }
        std::cout << "  +  ";
        for (const Gene &gene : parents.second){
            std::cout.width(2);
            std::cout << gene << " ";
        }
        std::cout << " ---> ";
        Genes child = getChild(parents);
        mutation(child);
        childPopulation.push_back(child);
        for (const Gene &gene : child){
            std::cout.width(2);
            std::cout << gene << " ";
        }
        std::cout.width(20);
        std::cout << "diff = ";
        std::cout.width(3);
        std::cout << getDiff(child);
        std::cout << "\n";
    }

    updateUtility(childPopulation);
    population = selection(childPopulation, _currentPopulation);
    return population;
}

void Generator::updateUtility(Population &population) {
    double sum = 0;
    unsigned int diff = 0;
    for (Genes &genes : population){
        for (size_t i = 0; i < genes.size(); ++i){
            diff += genes[i] * _coeffs[i];
        }
        diff = abs(_targetValue - diff);
        genes.diff = diff;
        sum += (1.0 / diff);
        if (diff == 0){
            genes.isBest = true;
        }
        diff = 0;
    }

    for (Genes &genes : population){
        genes.fitness = (1.0 / genes.diff) / sum;
    }
}

Parents Generator::getParents(const Population &population) {
    Parents parents;
    const Genes *parent1 = nullptr;
    const Genes *parent2 = nullptr;
    double value1 = 0;
    double value2 = 0;
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist100(1,100);
    for (const Genes &genes : population){
        double value = dist100(rng) * genes.fitness;
        if (value > value1){
            parent2 = parent1;
            value2 = value1;

            parent1 = &genes;
            value1 = value;
        } else if (value > value2){
            parent2 = &genes;
            value2 = value;
        }
    }

    parents.first = *parent1;
    parents.second = *parent2;
    return parents;
}

Genes Generator::getChild(const Parents &parents) {
    Genes child(parents.second);

    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> distPoint(1,_coeffs.size());
    unsigned int point = distPoint(rng);

    for (size_t i = 0; i < point; ++i){
        child[i] = parents.first[i];
    }

    return child;
}

unsigned int Generator::getDiff(const Genes &genes) {
    unsigned int diff = 0;
    for (size_t i = 0; i < genes.size(); ++i){
        diff += genes[i] * _coeffs[i];
    }
    diff = abs(_targetValue - diff);
    return diff;
}

void Generator::mutation(Genes &genes) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> distMutation(0,_coeffs.size()-1);
    std::uniform_int_distribution<std::mt19937::result_type> distTarget(0,_targetValue);
    genes[distMutation(rng)] = distTarget(rng);
}

Population Generator::selection(Population &population1, Population &population2){
    std::cout << "\nSelection...\n";
    Population newPopulation(population1);
    for (Genes &genes : population2){
        for (Genes &newGenes : newPopulation){
            if (genes.fitness > newGenes.fitness){
                newGenes = genes;
                break;
            }
        }
    }
    return newPopulation;
}
